from typing import List, Literal
from pydantic import BaseModel
from bson import ObjectId
from queries.users import UserOut
import pymongo
import os

client = pymongo.MongoClient(os.environ.get('DATABASE_URL'))
db = client['food-trucks']
trucks_collection = db['trucks'] # collection == table, the thing you query for data

Cuisines = Literal[
        "American",
        "Asian",
        "French",
        "Mediterranean",
        "Indian",
        "Italian",
        "Latin",
    ]

class TruckIn(BaseModel):
    name: str
    website: str
    category: Cuisines
    vegetarian_friendly: bool
    owner_id: int

class TruckOut(BaseModel):
    id: str
    name: str
    website: str
    category: Cuisines
    vegetarian_friendly: bool

class TruckListOut(BaseModel):
    trucks: list[TruckOut]

class TruckQueries:
    def get_all_trucks(self) -> List[TruckOut]:
        result_list = []
        for result in trucks_collection.find():
            self.convert_mongo_id_naming(result)
            result_list.append(result)
        return result_list

    def get_truck(self, truck_id: str) -> TruckOut | None:
        result = trucks_collection.find_one({"_id": ObjectId(truck_id)})
        if result:
            self.convert_mongo_id_naming(result)
        return result

    def delete_truck(self, truck_id: str) -> None:
        trucks_collection.delete_one({"_id": ObjectId(truck_id)})

    def create_truck(self, truck: TruckIn) -> TruckOut | None:
        result = trucks_collection.insert_one(truck.dict())
        return self.get_truck(result.inserted_id)
    
    def convert_mongo_id_naming(self, result):
        result["id"] = str(result["_id"])
